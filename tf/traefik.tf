resource "kubernetes_pod" "traefik" {
  metadata {
    name = "traefik"

    labels {
      App = "traefik"
    }
  }

  spec {
    container {
      image = "traefik:latest"
      name  = "traefik"
    }
  }
}

resource "kubernetes_service" "traefik" {
  metadata {
    name = "traefik"
  }

  spec {
    selector {
      App = "${kubernetes_pod.traefik.metadata.0.labels.App}"
    }

    port = [
      {
        port        = 80
        target_port = 80
        name = "http"
      },
      {
        port        = 443
        target_port = 443
        name = "https"
      },
      {
        port        = 8080
        target_port = 8080
        name = "http-alt"
      }
    ]

    type = "ClusterIP"
  }
}
